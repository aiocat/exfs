package main

import (
	"io/fs"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

// Exfs file struct.
type ExfsResult struct {
	Name, Mode, Time, Path string
	Size                   int64
}

// Search directory and find files.
func searchDir(regex, dir string, search_all bool) []*ExfsResult {
	var results []*ExfsResult

	if _, err := os.Stat(dir); os.IsNotExist(err) {
		giveErrorAndExit("Unknown Directory", "Directory not found.")
	}

	path, err := filepath.Abs(dir)

	if err != nil {
		giveErrorAndExit("Unknown Error", err.Error())
	}

	if search_all {
		filepath.Walk(dir, func(file_path string, info fs.FileInfo, _ error) error {
			found_path, err := filepath.Abs(file_path)

			if err != nil {
				giveErrorAndExit("Unknown Error", err.Error())
			}

			if info.IsDir() {
				return nil
			}

			return checkFile(&results, found_path, regex, info)
		})
	} else {
		allFiles, err := os.ReadDir(dir)

		if err != nil {
			giveErrorAndExit("Unknown Error", err.Error())
		}

		for _, i := range allFiles {
			if i.IsDir() {
				continue
			}

			file_info, err := i.Info()

			if err != nil {
				continue
			}

			checkFile(&results, filepath.Join(path, i.Name()), regex, file_info)
		}
	}

	return results
}

// Search a string in string slice.
func ssContains(s string, ss []string) bool {
	for i := 0; i < len(ss); i++ {
		if strings.EqualFold(s, ss[i]) {
			return true
		}
	}

	return false
}

// Check file and append if matches.
func checkFile(results *[]*ExfsResult, path, regex string, info fs.FileInfo) error {
	if info.IsDir() {
		return nil
	}

	if ok, _ := regexp.MatchString(regex, info.Name()); ok {
		*results = append(*results, &ExfsResult{
			Name: info.Name(),
			Mode: info.Mode().String(),
			Time: info.ModTime().String(),
			Path: path,
			Size: info.Size(),
		})
	}

	return nil
}
