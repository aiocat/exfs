package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/logrusorgru/aurora/v3"
)

func main() {
	args := os.Args[1:]
	var options []string

	// Check args length
	if len(args) < 1 {
		giveErrorAndExit("Argument Error", "Can't find a directory.")
	}

	directory, regex := parseArgs(&args, &options)
	results := searchDir(regex, directory, (ssContains("all", options) || ssContains("a", options)))

	// Check result length
	if len(results) < 1 {
		giveErrorAndExit("File Not Found", "Can't find any file.")
	}

	isMinimal, isColored := ssContains("m", options) || ssContains("minimal", options), ssContains("colorize", options) || ssContains("c", options)
	skipQuestions := ssContains("s", options) || ssContains("skip", options)

	var skipNum int

	if isMinimal {
		skipNum = 10
	} else {
		skipNum = 5
	}

	// Print all of the results.
	// Not used range keyword because it copies the array.
	for i := 0; i < len(results); i++ {
		var sizeColor, fileName, filePath, fileMode, fileTime aurora.Value

		// Colorize outputs if flag given.
		if isColored {
			if results[i].Size < 20971520 {
				sizeColor = aurora.Bold(aurora.BrightGreen(results[i].Size))
			} else if results[i].Size >= 20971520 && results[i].Size < 104857600 {
				sizeColor = aurora.Bold(aurora.BrightYellow(results[i].Size))
			} else {
				sizeColor = aurora.Bold(aurora.BrightRed(results[i].Size))
			}

			fileName = aurora.Bold(aurora.BrightWhite(results[i].Name))
			filePath = aurora.BrightWhite(results[i].Path)
			fileTime = aurora.Bold(aurora.Blue(results[i].Time))

			fileMode = aurora.Reset(results[i].Mode)
			fileMode = aurora.Reset(strings.ReplaceAll(fileMode.String(), "r", aurora.Underline(aurora.BrightGreen("r")).String()))
			fileMode = aurora.Reset(strings.ReplaceAll(fileMode.String(), "w", aurora.Underline(aurora.BrightCyan("w")).String()))
			fileMode = aurora.Reset(strings.ReplaceAll(fileMode.String(), "x", aurora.Underline(aurora.BrightRed("x")).String()))
		} else {
			fileName = aurora.Reset(results[i].Name)
			filePath = aurora.Reset(results[i].Path)
			sizeColor = aurora.Reset(results[i].Size)
			fileTime = aurora.Reset(results[i].Time)
			fileMode = aurora.Reset(results[i].Mode)
		}

		if isMinimal {
			fmt.Println(fileName, filePath, sizeColor, fileMode, fileTime)
		} else {
			fmt.Printf("%s [%s] (%d Byte)\n  Mode: %s\n  Time: %s\n",
				fileName,
				filePath,
				sizeColor,
				fileMode,
				fileTime,
			)
		}

		if i%skipNum == 0 && i != 0 && !skipQuestions {
			answer, err := askQuestion("Do you want to see more? (y/n): ")

			if err != nil {
				giveErrorAndExit("Unexcepted Error", err.Error())
			}

			if !(strings.EqualFold(answer, "y") || strings.EqualFold(answer, "yes") || strings.EqualFold(answer, "")) {
				break
			}
		}
	}
}
