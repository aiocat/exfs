package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/logrusorgru/aurora/v3"
)

// Parse args and return directory & regex.
func parseArgs(args, options *[]string) (string, string) {
	var new_args []string

	// Parse options.
	for i := 0; i < len(*args); i++ {
		if strings.HasPrefix((*args)[i], "-") {
			*options = append(*options, strings.TrimLeft((*args)[i], "-"))
		} else {
			new_args = append(new_args, (*args)[i])
		}
	}

	*args = new_args

	if len(*args) == 1 {
		return ".", (*args)[0]
	} else if len(*args) > 1 {
		return (*args)[0], (*args)[1]
	}

	return "", ""
}

// Print an error and exit.
func giveErrorAndExit(t, desc string) {
	fmt.Printf("Found Exception (%s): %s\n", aurora.Bold(aurora.BrightRed(t)), aurora.BrightWhite(desc))
	os.Exit(-1)
}

// Ask a question and wait for input.
func askQuestion(q string) (string, error) {
	fmt.Print(q)

	reader := bufio.NewReader(os.Stdin)
	result, err := reader.ReadString('\n')

	if err != nil {
		return "", err
	}

	return strings.TrimSpace(result), nil
}
