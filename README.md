# exfs

Minimal, free and open-source regex file search tool.

## Short Description

Exfs (Reg**ex** **F**ile **S**earch) is a simple and minimal tool for search one or more file in directory with regex.

## Installation

Get the binary from release page and add to your path.

## Usage

### Simple Usage

- `exfs ".*\.txt"` (arg1 is regex, directory is where app runs.)
- `exfs "path/to/directory" ".*\.txt"` (arg1 is directory, arg2 is regex.)

### Flags

- (`-s` | `-skip`): Skip list confirmation.
- (`-m` | `-minimal`): Lists in minimal mode.
- (`-a` | `-all`): Also walks on sub-directories and gets the results.
- (`-c` | `-colorize`): Colorize the output. (Doesn't works on Windows cmd.)

## Screenshot

![](https://i.imgur.com/tZLvqFU.png)

## Authors

- [Aiocat](https://gitlab.com/aiocat)

## License

This project is distributed under MIT license.

## Project status

Under development.
